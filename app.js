const express = require('express')
const path = require('path')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
const favicon = require('serve-favicon')
const cookieParser = require('cookie-parser')
const helmet = require('helmet')
const morgan = require('morgan')

const app = express()

// Swagger
const swaggerUI = require('swagger-ui-express')
const apiDocumentation = require('./api/apidocs.json')
app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocumentation))

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')
app.use(fileUpload())
app.use(helmet.frameguard())
app.use(helmet.xssFilter())
app.use(helmet.noSniff())
app.disable('x-powered-by')
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))

// Logger
app.use(morgan('[:method] :remote-user [:date[web]] - [statusCode: :status] - :url :response-time'))

// Call Router
app.use('/daerah', require('./routes/daerah'))
app.use('/covid', require('./routes/covid'))

app.listen(3131, function () {
  console.log('Server started on http://localhost:3131')
})

module.exports = app
