const express = require('express')
const router = express.Router()
const hitApi = require('../api/hit')

router.get('/', (req, res) => {
  const dataApiCovid = {
    url: ''
  }
  hitApi.reqDataApiCovid(dataApiCovid, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/indonesia', (req, res) => {
  const dataApiCovid = {
    url: '/indonesia'
  }
  hitApi.reqDataApiCovid(dataApiCovid, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/positif', (req, res) => {
  const dataApiCovid = {
    url: '/positif'
  }
  hitApi.reqDataApiCovid(dataApiCovid, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('cd /sembuh', (req, res) => {
  const dataApiCovid = {
    url: '/sembuh'
  }
  hitApi.reqDataApiCovid(dataApiCovid, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('cd /meninggal', (req, res) => {
  const dataApiCovid = {
    url: '/meninggal'
  }
  hitApi.reqDataApiCovid(dataApiCovid, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

module.exports = router
