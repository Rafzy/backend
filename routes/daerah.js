const express = require('express')
const router = express.Router()
const hitApi = require('../api/hit')

router.get('/', (req, res) => {
  const reqDataApi = {
    url: '/api/daerahindonesia/provinsi'
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    res.render('index', {
      data: response.data.provinsi
    })
  })
})

router.get('/provinsi', (req, res) => {
  const reqDataApi = {
    url: '/api/daerahindonesia/provinsi'
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data.provinsi,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/provinsi/:id', (req, res) => {
  const idProvinsi = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/provinsi/${idProvinsi}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kota/:id', (req, res) => {
  const idProvinsi = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kota?id_provinsi=${idProvinsi}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data.kota_kabupaten,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kota/detail/:id', (req, res) => {
  const idCity = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kota/${idCity}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kecamatan/:id', (req, res) => {
  const idCity = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kecamatan?id_kota=${idCity}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data.kecamatan,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kecamatan/detail/:id', (req, res) => {
  const idDistrict = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kecamatan/${idDistrict}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kelurahan/:id', (req, res) => {
  const idDistrict = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan?id_kecamatan=${idDistrict}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data.kelurahan,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

router.get('/kelurahan/detail/:id', (req, res) => {
  const idSubDistrict = req.params.id
  const reqDataApi = {
    url: `/api/daerahindonesia/kelurahan/${idSubDistrict}`
  }
  hitApi.reqDataApi(reqDataApi, (response) => {
    if (response.status === 200) {
      res.status(200).send({
        status: res.statusCode,
        data: response.data,
        message: 'Success get data'
      })
    } else {
      res.status(400).send({
        status: res.statusCode,
        data: [],
        message: 'Error get data'
      })
    }
  })
})

module.exports = router
